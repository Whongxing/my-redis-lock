package locktest.myredislock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class MyRedisLockApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyRedisLockApplication.class, args);
    }

}
