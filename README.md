# my-redis-lock

#### 介绍
采用Reids实现的一个简单分布式锁
【描述】：Reids如何做实现一个分布式锁

#### 软件架构
SpringBoot简单的一个模拟减库存接口


#### 安装教程

1.  JMeter压测工具  参考： https://www.cnblogs.com/monjeo/p/9330464.html
2.  Nginx 做一个负载均衡   下载安装参考： https://www.cnblogs.com/jiangwangxiang/p/8481661.html
我自己的Config没怎么改动，就代理了一个端口，如下
```
upstream tomcatserver{
    server 192.168.48.150:8080 weight=1;
    server 192.168.48.150:8090 weight=1;
}
server{
    location / {
                proxy_pass   http://tomcatserver;
                proxy_redirect default;
    }
}
```
3、 安装Redis，配置Boot的配置文件
#### 使用说明

1.  在redis存一个代表库存量的提前key value ，我自己 number "200"(字符串)
2.  简单代码可以看一下(逻辑，不加锁)
```
            int number = jedis.get("number"));
            if(number>0){
                int realNumber = number - 1;
                jedis.setset("number",realNumber+"");
                System.out.println("减库存成功，剩余："+realNumber);
            }else{
                System.out.println("减库存失败，库存不足");
            }
```
3.  IDEA，在项目Config中勾选Allow parallel run（这样只需要改个端口就可以在不同端口启动两个接口）

####  理解问题

1、不加锁，多个请同时读，超卖了?

2、synchrized  JVM级别的锁, 分布式不起作用？

3、用分布式锁，利用redis的setnx命令（如果有返回false,什么都不做，没有才set）

- 3.1 产生的问题

 如果执行业务代码异常，无法释放锁？  ——try finally可以解决

 执行业务代码挂了，执行不了Finally?  ——超时时间可以解决

- 3.2 超时时间引发的新问题

线程2可以删除线程1的锁   ——uuID一定程度可以解决

判断了UUID,这个时候线程1卡顿，锁超时了，线程2加锁了，线程1还是会删除2的锁

- 3.3 终极方案

锁续命

####  新东西

了解到Redisson , 特别好用。




